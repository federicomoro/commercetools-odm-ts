import BaseResource from './baseresource';
import Reference from './fieldTypes/Reference';
import CustomField from './fieldTypes/CustomField';
import Asset from './fieldTypes/Asset';
declare class Category extends BaseResource {
    key?: string;
    name?: object;
    slug?: object;
    description?: object;
    ancestors?: Array<Reference>;
    parent?: Reference;
    orderHint: string;
    externalId?: string;
    metaTitle?: object;
    metaDescription?: object;
    metaKeywords?: object;
    custom?: CustomField;
    assets?: Array<Asset>;
    static endPoint: string;
    static syncActions: any;
}
export default Category;
