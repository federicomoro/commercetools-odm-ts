import BaseResource from './baseresource';
declare class CustomObject extends BaseResource {
    container: string;
    key: string;
    value: {};
    static endPoint: string;
    save(): any;
}
export default CustomObject;
