import Connection from './connection';
declare class CTODM {
    static connection: Connection;
    static models: any;
    static connect(config: any): void;
    static model(name: string, cls: any): void;
}
export default CTODM;
