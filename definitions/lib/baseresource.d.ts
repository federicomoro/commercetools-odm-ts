import Connection from './connection';
declare class BaseResource {
    protected static _cls: any;
    protected static endPoint: string;
    protected static client: any;
    protected static syncActions?: any;
    protected static requestBuilder?: any;
    id?: string;
    version?: number;
    password?: string;
    addresses: Array<any>;
    shippingAddressIds: Array<string>;
    billingAddressIds: Array<string>;
    isEmailVerified: boolean;
    companyName?: string;
    createdAt: Date;
    lastModifiedAt: Date;
    lastMessageSequenceNumber?: number;
    static connect(connection: Connection): void;
    protected static service(): any;
    static _getUri(args: {
        where?: string;
        expand?: string;
        page?: number;
        perPage?: number;
    }): any;
    static get(id: string, expand?: string): Promise<BaseResource>;
    static find(where?: string, expand?: string, page?: number, perPage?: number): any;
    static findOne(where?: string, expand?: string): Promise<BaseResource>;
    protected assign(init?: Partial<BaseResource>): void;
    constructor(init?: Partial<BaseResource>);
    save(): any;
    delete(): any;
}
export default BaseResource;
