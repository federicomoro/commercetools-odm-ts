import Reference from './reference';
declare class CustomField {
    type: Reference;
    fields?: any;
}
export default CustomField;
