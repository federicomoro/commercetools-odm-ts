import CustomField from './customfield';
declare class Asset {
    id?: string;
    name: object;
    description?: object;
    tag?: Array<string>;
    custom?: CustomField;
}
export default Asset;
