declare class Reference {
    typeId: string;
    id: string;
    obj?: any;
}
export default Reference;
