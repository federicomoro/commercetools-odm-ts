import BaseResource from './baseresource';
declare class Pagination {
    offset: number;
    count: number;
    total: number;
    results: Array<BaseResource>;
    private cls;
    private where?;
    private expand?;
    private page;
    private perPage;
    constructor(cls: any, init?: Partial<Pagination>, where?: string, expand?: string, page?: number, perPage?: number);
    assign(init: Partial<Pagination>): void;
    next(): Promise<Pagination>;
    hasNext(): boolean;
    private find();
}
export default Pagination;
