import BaseResource from './baseresource';
declare class Customer extends BaseResource {
    customerNumber?: string;
    email: string;
    key: string;
    firstName?: string;
    middleName?: string;
    lastName?: string;
    static endPoint: string;
    static syncActions: any;
}
export default Customer;
