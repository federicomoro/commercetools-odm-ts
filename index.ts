import Connection from './lib/connection';
import Category from './lib/category';
import CustomObject from './lib/customObject';
import Customer from './lib/customer';

class CTODM {
    static connection: Connection;
    static models: any = {
        Category: Category,
        CustomObject: CustomObject,
        Customer: Customer,
    };
    static connect(config: any) {
        this.connection = new Connection(config);
        for (let model in this.models) {
            this.models[model].connect(this.connection);
        }
    }

    static model(name: string, cls: any) {
        cls.connect(this.connection);
        this.models[name] = cls;
    }
}

export default CTODM;