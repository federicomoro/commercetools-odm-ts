declare module '@commercetools/api-request-builder';
declare module '@commercetools/sync-actions';
declare module '@commercetools/sdk-client';
declare module '@commercetools/sdk-middleware-auth';
declare module '@commercetools/sdk-middleware-http';
declare module '@commercetools/sdk-middleware-queue';
declare module '@commercetools/sdk-middleware-user-agent';