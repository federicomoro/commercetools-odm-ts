"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = require("./lib/connection");
var category_1 = require("./lib/category");
var customObject_1 = require("./lib/customObject");
var customer_1 = require("./lib/customer");
var CTODM = /** @class */ (function () {
    function CTODM() {
    }
    CTODM.connect = function (config) {
        this.connection = new connection_1.default(config);
        for (var model in this.models) {
            this.models[model].connect(this.connection);
        }
    };
    CTODM.model = function (name, cls) {
        cls.connect(this.connection);
        this.models[name] = cls;
    };
    CTODM.models = {
        Category: category_1.default,
        CustomObject: customObject_1.default,
        Customer: customer_1.default,
    };
    return CTODM;
}());
exports.default = CTODM;
