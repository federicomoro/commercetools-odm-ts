"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Pagination = /** @class */ (function () {
    function Pagination(cls, init, where, expand, page, perPage) {
        if (page === void 0) { page = 1; }
        if (perPage === void 0) { perPage = 20; }
        this.results = new Array();
        //this.results = new Array();
        this.cls = cls;
        if (init && init.results) {
            for (var _i = 0, _a = init.results; _i < _a.length; _i++) {
                var result = _a[_i];
                this.results.push(new cls(result));
            }
            delete init.results;
        }
        Object.assign(this, init);
        this.where = where;
        this.expand = expand;
        this.page = page;
        this.perPage = perPage;
    }
    Pagination.prototype.assign = function (init) {
        if (init && init.results) {
            for (var _i = 0, _a = init.results; _i < _a.length; _i++) {
                var result = _a[_i];
                this.results.push(new this.cls(result));
            }
            delete init.results;
        }
        Object.assign(this, init);
    };
    Pagination.prototype.next = function () {
        var _this = this;
        if (this.hasNext()) {
            this.page++;
            return this.find().then(function (results) { return _this; });
        }
        else {
            throw new Error('No more pages');
        }
    };
    Pagination.prototype.hasNext = function () {
        console.log('page', this.page, 'perPage', this.perPage);
        return this.page * this.perPage < this.total;
    };
    Pagination.prototype.find = function () {
        var _this = this;
        var uri = this.cls._getUri({ where: this.where, expand: this.expand, page: this.page, perPage: this.perPage }).build();
        return this.cls.client.execute({ uri: uri, method: 'GET' }).then(function (result) {
            return _this.assign(result.body);
            //result.body
        });
    };
    return Pagination;
}());
exports.default = Pagination;
