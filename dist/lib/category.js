"use strict";
// import { createSyncCustomers } from '@commercetools/sync-actions';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var createSyncCategories = require('@commercetools/sync-actions').createSyncCategories;
var baseresource_1 = require("./baseresource");
var Category = /** @class */ (function (_super) {
    __extends(Category, _super);
    function Category() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Category.endPoint = 'categories';
    Category.syncActions = createSyncCategories();
    return Category;
}(baseresource_1.default));
exports.default = Category;
