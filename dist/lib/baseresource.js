"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
import { createRequestBuilder } from '@commercetools/api-request-builder';
import { createSyncProducts } from '@commercetools/sync-actions';
*/
var createRequestBuilder = require('@commercetools/api-request-builder').createRequestBuilder;
var createSyncProducts = require('@commercetools/sync-actions').createSyncProducts;
var pagination_1 = require("./pagination");
var BaseResource = /** @class */ (function () {
    function BaseResource(init) {
        Object.assign(this, init);
    }
    BaseResource.connect = function (connection) {
        this.client = connection.client;
        this.requestBuilder = connection.requestBuilder;
    };
    BaseResource.service = function () {
        return this.requestBuilder[this.endPoint];
    };
    BaseResource._getUri = function (args) {
        var uri = this.service();
        if (args.where) {
            uri = uri.where(args.where);
        }
        if (args.expand) {
            uri = uri.expand(args.where);
        }
        if (args.page) {
            if (args.page < 0) {
                throw new Error("page argument must be a number >= 1.' " + args.page + " given");
            }
            uri = uri.page(args.page);
        }
        if (args.perPage) {
            if (args.perPage < 0) {
                throw new Error("perPage argument must be a number >= 1. " + args.perPage + " given");
            }
            uri = uri.perPage(args.perPage);
        }
        return uri;
    };
    BaseResource.get = function (id, expand) {
        var _this = this;
        var uri = this._getUri({ expand: expand })
            .byId(id)
            .build();
        return this.client.execute({ uri: uri, method: 'GET' }).then(function (result) { return new _this(result.body); });
    };
    BaseResource.find = function (where, expand, page, perPage) {
        var _this = this;
        if (page === void 0) { page = 1; }
        if (perPage === void 0) { perPage = 20; }
        var uri = this._getUri({ where: where, expand: expand, page: page, perPage: perPage }).build();
        return this.client.execute({ uri: uri, method: 'GET' }).then(function (result) {
            return new pagination_1.default(_this, result.body, where, expand, page, perPage);
            //result.body
        });
    };
    BaseResource.findOne = function (where, expand) {
        var _this = this;
        var uri = this._getUri({ where: where, expand: expand }).build();
        return this.client.execute({ uri: uri, method: 'GET' }).then(function (result) { return new _this(result.body.results[0]); });
    };
    BaseResource.prototype.assign = function (init) {
        Object.assign(this, init);
    };
    BaseResource.prototype.save = function () {
        var _this = this;
        var _cls = this.constructor;
        var uri = _cls._getUri({});
        if (this.id) {
            return _cls.get(this.id).then(function (resource) {
                uri = uri.byId(_this.id).build();
                var actions = _cls.syncActions.buildActions(_this, resource);
                return _cls.client.execute({ uri: uri, method: 'POST', body: JSON.stringify({ version: resource.version, actions: actions }) }).then((function (result) {
                    _this.assign(result.body);
                    return new _cls(result.body);
                }));
            });
        }
        else {
            uri = uri.build();
            return _cls.client.execute({ uri: uri, method: 'POST', body: JSON.stringify(this) }).then((function (result) {
                _this.assign(result.body);
                return new _cls(result.body);
            }));
        }
    };
    BaseResource.prototype.delete = function () {
        var _this = this;
        if (!this.id) {
            return new Error('Object has no ID field');
        }
        var _cls = this.constructor;
        var uri = _cls._getUri({})
            .byId(this.id)
            .build();
        return _cls.client.execute({ uri: uri, method: 'DELETE' }).then((function (result) {
            delete _this.id;
            Object.freeze(_this);
            return null;
        }));
    };
    return BaseResource;
}());
exports.default = BaseResource;
