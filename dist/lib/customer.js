"use strict";
// import { createSyncCustomers } from '@commercetools/sync-actions';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var createSyncCustomers = require('@commercetools/sync-actions').createSyncCustomers;
var baseresource_1 = require("./baseresource");
var Customer = /** @class */ (function (_super) {
    __extends(Customer, _super);
    function Customer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Customer.endPoint = 'customers';
    Customer.syncActions = createSyncCustomers();
    return Customer;
}(baseresource_1.default));
exports.default = Customer;
