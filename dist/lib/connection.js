"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
import { createClient } from '@commercetools/sdk-client';
import { createAuthMiddlewareForClientCredentialsFlow } from '@commercetools/sdk-middleware-auth';
import { createHttpMiddleware } from '@commercetools/sdk-middleware-http';
import { createQueueMiddleware } from '@commercetools/sdk-middleware-queue';
import { createUserAgentMiddleware } from '@commercetools/sdk-middleware-user-agent';
import { createRequestBuilder } from '@commercetools/api-request-builder';
import { createSyncProducts } from '@commercetools/sync-actions';
*/
var createClient = require('@commercetools/sdk-client').createClient;
var createAuthMiddlewareForClientCredentialsFlow = require('@commercetools/sdk-middleware-auth').createAuthMiddlewareForClientCredentialsFlow;
var createHttpMiddleware = require('@commercetools/sdk-middleware-http').createHttpMiddleware;
var createQueueMiddleware = require('@commercetools/sdk-middleware-queue').createQueueMiddleware;
var createUserAgentMiddleware = require('@commercetools/sdk-middleware-user-agent').createUserAgentMiddleware;
var createRequestBuilder = require('@commercetools/api-request-builder').createRequestBuilder;
var createSyncProducts = require('@commercetools/sync-actions').createSyncProducts;
var Connection = /** @class */ (function () {
    function Connection(config) {
        this.client = createClient({
            // The order of the middlewares is important !!!
            middlewares: [
                createAuthMiddlewareForClientCredentialsFlow(config.auth),
                /* createAuthMiddlewareForClientCredentialsFlow({
                  host: 'https://auth.commercetools.co',
                  projectKey: 'stylex-test',
                  credentials: {
                    clientId: 'IFG9E--ZBjBtTNHgqH2aHjBn',
                    clientSecret: 'tSiV_2afGh-4Apw799rXqAdkj8KYDebD',
                  },
                }), */
                createQueueMiddleware({ concurrency: 10 }),
                createHttpMiddleware(config.middleware),
            ],
        });
        // this.requestBuilder = createRequestBuilder({ projectKey: config.commercetools.projectKey });
        this.requestBuilder = createRequestBuilder({ projectKey: config.auth.projectKey });
    }
    return Connection;
}());
exports.default = Connection;
