"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var baseresource_1 = require("./baseresource");
var CustomObject = /** @class */ (function (_super) {
    __extends(CustomObject, _super);
    function CustomObject() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /*constructor(init?: Partial<CustomObject>) {
      super(init);
    }*/
    CustomObject.prototype.save = function () {
        var _this = this;
        var _cls = this.constructor;
        var uri = CustomObject._getUri({}).build();
        return CustomObject.client.execute({ uri: uri, method: 'POST', body: JSON.stringify(this) }).then((function (result) {
            _this.assign(result.body);
            return new _cls(result.body);
        }));
    };
    CustomObject.endPoint = 'customObjects';
    return CustomObject;
}(baseresource_1.default));
exports.default = CustomObject;
