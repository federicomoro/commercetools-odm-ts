import Connection from './connection';
import BaseResource from './baseresource';

class CustomObject extends BaseResource {
    container: string;
    key: string;
    value: {};
    static endPoint = 'customObjects';

  /*constructor(init?: Partial<CustomObject>) {
    super(init);
  }*/

  save() {
    const _cls: any = this.constructor;
    const uri = CustomObject._getUri({}).build();
    return CustomObject.client.execute({ uri, method: 'POST', body: JSON.stringify(this) }).then(((result: any) => {
      this.assign(result.body);
      return new _cls(result.body);
    }));
  }
}

export default CustomObject;
