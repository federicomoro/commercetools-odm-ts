import Connection from './connection';
import BaseResource from './baseresource';
declare class CustomObject extends BaseResource {
    container: string;
    key: string;
    value: {};
    static init(connection: Connection): typeof BaseResource;
    save(): any;
}
export default CustomObject;
