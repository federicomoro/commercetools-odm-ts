/*
import { createClient } from '@commercetools/sdk-client';
import { createAuthMiddlewareForClientCredentialsFlow } from '@commercetools/sdk-middleware-auth';
import { createHttpMiddleware } from '@commercetools/sdk-middleware-http';
import { createQueueMiddleware } from '@commercetools/sdk-middleware-queue';
import { createUserAgentMiddleware } from '@commercetools/sdk-middleware-user-agent';
import { createRequestBuilder } from '@commercetools/api-request-builder';
import { createSyncProducts } from '@commercetools/sync-actions';
*/
const createClient = require('@commercetools/sdk-client').createClient;
const createAuthMiddlewareForClientCredentialsFlow = require('@commercetools/sdk-middleware-auth').createAuthMiddlewareForClientCredentialsFlow;
const createHttpMiddleware = require('@commercetools/sdk-middleware-http').createHttpMiddleware;
const createQueueMiddleware = require('@commercetools/sdk-middleware-queue').createQueueMiddleware;
const createUserAgentMiddleware = require('@commercetools/sdk-middleware-user-agent').createUserAgentMiddleware;
const createRequestBuilder = require('@commercetools/api-request-builder').createRequestBuilder;
const createSyncProducts = require('@commercetools/sync-actions').createSyncProducts;

class Connection {
  client: any;
  requestBuilder: any;

  constructor(config: any) {
    this.client = createClient({
      // The order of the middlewares is important !!!
      middlewares: [
        createAuthMiddlewareForClientCredentialsFlow(config.auth),
        /* createAuthMiddlewareForClientCredentialsFlow({
          host: 'https://auth.commercetools.co',
          projectKey: 'stylex-test',
          credentials: {
            clientId: 'IFG9E--ZBjBtTNHgqH2aHjBn',
            clientSecret: 'tSiV_2afGh-4Apw799rXqAdkj8KYDebD',
          },
        }), */
        createQueueMiddleware({ concurrency: 10 }),
        createHttpMiddleware(config.middleware),
        // createHttpUserAgent({
        //   libraryName: 'customizer-dao',
        //   libraryVersion: '1.0.0',
        //   contactUrl: 'https://github.com/commercetools/nodejs',
        //   contactEmail: 'npmjs@commercetools.com',
        // }),
      ],
    });
    // this.requestBuilder = createRequestBuilder({ projectKey: config.commercetools.projectKey });
    this.requestBuilder = createRequestBuilder({ projectKey: config.auth.projectKey });
  }
}

export default Connection;
