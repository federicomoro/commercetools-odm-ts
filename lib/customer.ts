// import { createSyncCustomers } from '@commercetools/sync-actions';

const createSyncCustomers = require('@commercetools/sync-actions').createSyncCustomers;
import Connection from './connection';
import BaseResource from './baseresource';

class Customer extends BaseResource {
  customerNumber?: string;
  email: string;
  key: string;
  firstName?: string;
  middleName?: string;
  lastName?: string;

  static endPoint = 'customers';
  static syncActions = createSyncCustomers();

  /*constructor(init?: Partial<Customer>) {
    super(init);
  }*/
}

export default Customer;