"use strict";
exports.__esModule = true;
/*
import { createRequestBuilder } from '@commercetools/api-request-builder';
import { createSyncProducts } from '@commercetools/sync-actions';
*/
var createRequestBuilder = require('@commercetools/api-request-builder').createRequestBuilder;
var createSyncProducts = require('@commercetools/sync-actions').createSyncProducts;
var BaseResource = /** @class */ (function () {
    function BaseResource(params) {
        for (var key in params) {
            this[key] = params[key];
        }
    }
    BaseResource.init = function (connection) {
        BaseResource.service = BaseResource.endPoint ? connection.requestBuilder[BaseResource.endPoint] : null;
        BaseResource.client = connection.client;
        return this;
    };
    BaseResource._getUri = function (where, expand, page, perPage) {
        var uri = this.service;
        if (where) {
            uri = uri.where(where);
        }
        if (expand) {
            uri = uri.expand(where);
        }
        if (page) {
            if (page < 0) {
                throw new Error("page argument must be a number >= 1.' " + page + " given");
            }
            uri = uri.page(page);
        }
        if (perPage) {
            if (perPage < 0) {
                throw new Error("perPage argument must be a number >= 1. " + perPage + " given");
            }
            uri = uri.perPage(perPage);
        }
        return uri;
    };
    BaseResource.get = function (id, expand) {
        var uri = this._getUri(expand = expand)
            .byId(id)
            .build();
        return this.client.execute({ uri: uri, method: 'GET' }).then(function (result) { return new BaseResource(result.body); });
    };
    BaseResource.find = function (where, expand, page, perPage) {
        var uri = this._getUri(where, expand, page, perPage).build();
        return this.client.execute({ uri: uri, method: 'GET' }).then(function (result) { return result.body; });
    };
    BaseResource.findOne = function (where, expand) {
        var uri = this._getUri(where = where, expand = expand).build();
        return this.client.execute({ uri: uri, method: 'GET' }).then(function (result) { return result.body.results[0]; });
    };
    BaseResource.prototype.save = function () {
        var _this = this;
        console.log(this);
        var uri = BaseResource.service;
        if (this.id) {
            return BaseResource.get(this.id).then(function (resource) {
                uri = uri.byId(_this.id).build();
                console.log(BaseResource.syncActions.buildActions);
                console.log(_this);
                var actions = BaseResource.syncActions.buildActions(_this, resource);
                console.log(actions);
                return BaseResource.client.execute({ uri: uri, method: 'POST', body: JSON.stringify({ version: resource.version, actions: actions }) }).then((function (result) { return result.body; }));
            });
        }
        else {
            uri = uri.build();
            return BaseResource.client.execute({ uri: uri, method: 'POST', body: JSON.stringify(this) }).then((function (result) { return result.body; }));
        }
    };
    return BaseResource;
}());
exports["default"] = BaseResource;
