/*
import { createRequestBuilder } from '@commercetools/api-request-builder';
import { createSyncProducts } from '@commercetools/sync-actions';
*/
const createRequestBuilder = require('@commercetools/api-request-builder').createRequestBuilder;
const createSyncProducts = require('@commercetools/sync-actions').createSyncProducts;
import Connection from './connection';
import Pagination from './pagination';

class BaseResource {
  protected static _cls: any;
  protected static endPoint: string;
  protected static client: any;
  protected static syncActions?: any;
  protected static requestBuilder? : any;

  id?: string;
  version?: number;
  password?: string;
  addresses: Array<any>;
  shippingAddressIds: Array<string>;
  billingAddressIds: Array<string>;
  isEmailVerified: boolean;
  companyName?: string;
  createdAt: Date;
  lastModifiedAt: Date;
  lastMessageSequenceNumber?: number;

  static connect(connection: Connection) {
    this.client = connection.client;
    this.requestBuilder = connection.requestBuilder;
  }

  protected static service() {
    return this.requestBuilder[this.endPoint];
  }

  static _getUri(args: { where?: string, expand?: string, page?: number, perPage?: number }) {
    let uri = this.service();
    if (args.where) {
      uri = uri.where(args.where);
    }

    if (args.expand) {
      uri = uri.expand(args.where);
    }

    if (args.page) {
      if (args.page < 0) {
        throw new Error(`page argument must be a number >= 1.' ${args.page} given`);
      }
      uri = uri.page(args.page);
    }

    if (args.perPage) {
      if (args.perPage < 0) {
        throw new Error(`perPage argument must be a number >= 1. ${args.perPage} given`);
      }
      uri = uri.perPage(args.perPage);
    }

    return uri;
  }

  static get(id: string, expand?: string) : Promise<BaseResource> {
    const uri = this._getUri({expand: expand})
      .byId(id)
      .build();

    return this.client.execute({ uri, method: 'GET' }).then((result: any) => new this(result.body));
  }

  static find(where?: string, expand?: string, page: number = 1, perPage: number = 20) {
    const uri = this._getUri({where, expand, page, perPage}).build();
    return this.client.execute({ uri, method: 'GET' }).then((result: any) => {
      return new Pagination(this, result.body, where, expand, page, perPage);
      //result.body
    });
  }

  static findOne(where?: string, expand?: string) : Promise<BaseResource> {
    const uri = this._getUri({where, expand}).build();
    return this.client.execute({ uri, method: 'GET' }).then((result: any) => new this(result.body.results[0]));
  }

  protected assign(init?: Partial<BaseResource>) {
    Object.assign(this, init);
  }

  constructor(init?: Partial<BaseResource>) {
    Object.assign(this, init);
  }

  save() {
    const _cls: any = this.constructor;
    let uri = _cls._getUri({});

    if (this.id) {
      return _cls.get(this.id).then((resource: any) => {
        uri = uri.byId(this.id).build();
        const actions = _cls.syncActions.buildActions(this, resource);
        return _cls.client.execute({ uri, method: 'POST', body: JSON.stringify({ version: resource.version, actions }) }).then(((result: any) => {
          this.assign(result.body);
          return new _cls(result.body);
        }));
      });
    } else {
      uri = uri.build();
      return _cls.client.execute({ uri, method: 'POST', body: JSON.stringify(this) }).then(((result: any) =>  {
        this.assign(result.body);
        return new _cls(result.body);
      }));
    }
  }

  delete() {
    if (!this.id) {
      return new Error('Object has no ID field');
    }
    const _cls: any = this.constructor;
    const uri = _cls._getUri({})
      .byId(this.id)
      .build();
    return _cls.client.execute({ uri, method: 'DELETE'}).then(((result: any) => {
      delete this.id;
      Object.freeze(this);
      return null;
    }));
  }
}

export default BaseResource;
