import Connection from './connection';
declare class BaseResource {
    protected static endPoint: string;
    static service: any;
    protected static client: any;
    protected static syncActions?: any;
    id?: string;
    version?: number;
    password?: string;
    addresses: Array<any>;
    shippingAddressIds: Array<string>;
    billingAddressIds: Array<string>;
    isEmailVerified: boolean;
    companyName?: string;
    createdAt: Date;
    lastModifiedAt: Date;
    lastMessageSequenceNumber?: number;
    [key: string]: any;
    static init(connection: Connection): typeof BaseResource;
    static _getUri(where?: string, expand?: string, page?: number, perPage?: number): any;
    static get(id: string, expand?: string): Promise<BaseResource>;
    static find(where?: string, expand?: string, page?: number, perPage?: number): any;
    static findOne(where?: string, expand?: string): BaseResource;
    constructor(params: any);
    save(): any;
}
export default BaseResource;
