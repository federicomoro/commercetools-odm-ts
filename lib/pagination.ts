import BaseResource from './baseresource';

class Pagination {
    offset: number;
    count: number;
    total: number;
    results: Array<BaseResource> = new Array();

    private cls: any;
    private where?: string;
    private expand?: string;
    private page: number;
    private perPage: number;

    constructor(cls: any, init?: Partial<Pagination>, where?: string, expand?: string, page: number = 1, perPage: number = 20) {
        //this.results = new Array();
        this.cls = cls;
        if (init && init.results) {
            for (let result of init.results) {
                this.results.push(new cls(result));
            }
            delete init.results;
        }
        Object.assign(this, init);
        this.where = where;
        this.expand = expand;
        this.page = page;
        this.perPage = perPage;
    }

    assign(init: Partial<Pagination>) {
        if (init && init.results) {
            for (let result of init.results) {
                this.results.push(new this.cls(result));
            }
            delete init.results;
        }
        Object.assign(this, init);
    }

    next(): Promise<Pagination> {
        if (this.hasNext()) {
            this.page++;
            return this.find().then((results: Array<BaseResource>) => this );
        } else {
            throw new Error('No more pages');
        }
    }

    hasNext() : boolean {
        console.log('page', this.page, 'perPage', this.perPage);
        return this.page * this.perPage < this.total;
    }

    private find() {
        const uri = this.cls._getUri({where: this.where, expand: this.expand, page: this.page, perPage: this.perPage}).build();
        return this.cls.client.execute({ uri, method: 'GET' }).then((result: any) => {
            return this.assign(result.body);
          //result.body
        });
      }
}

export default Pagination;