"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var baseresource_1 = require("./baseresource");
var CustomObject = /** @class */ (function (_super) {
    __extends(CustomObject, _super);
    function CustomObject() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CustomObject.init = function (connection) {
        CustomObject.endPoint = 'customObjects';
        return _super.init.call(this, connection);
    };
    CustomObject.prototype.save = function () {
        var uri = CustomObject.service.build();
        console.log(JSON.stringify(this));
        return CustomObject.client.execute({ uri: uri, method: 'POST', body: JSON.stringify(this) }).then((function (result) { return result.body; }));
    };
    return CustomObject;
}(baseresource_1["default"]));
exports["default"] = CustomObject;
