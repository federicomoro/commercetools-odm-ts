import Connection from './connection';
import BaseResource from './baseresource';
declare class Customer extends BaseResource {
    customerNumber?: string;
    email: string;
    key: string;
    firstName?: string;
    middleName?: string;
    lastName?: string;
    static init(connection: Connection): typeof BaseResource;
    _draft(): {
        customerNumber: string;
        email: string;
        key: string;
        value: {};
    };
}
export default Customer;
