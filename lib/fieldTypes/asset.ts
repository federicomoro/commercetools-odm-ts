import CustomField from './customfield';

class Asset {
    id?: string;
    // sources - Array of AssetSource - Has at least one entry
    name: object;
    description?: object;
    tag?: Array<string>;
    custom?: CustomField;
}

export default Asset;