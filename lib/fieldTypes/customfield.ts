import Reference from './reference';

class CustomField {
    type: Reference;
    fields?: any;
}

export default CustomField;